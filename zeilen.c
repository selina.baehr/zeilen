#include "datastorage.h"
#include <stdio.h>
#include <stddef.h>
#include <wchar.h>
#include <unistd.h>
#include <termios.h>
#include <locale.h>
#include <sys/ioctl.h>
static struct termios old_tio; /* In order to restore at exit.*/
void pri(int x, int y, wchar_t* w){
	wprintf(L"\e[%d;%dH",x,y);
	wprintf(L"%ls",w);
}

int getWindowSize(){
	struct winsize ws;
	ioctl(1, TIOCGWINSZ, &ws);
	printf("Screen width: %i  Screen height: %i\n", ws.ws_col, ws.ws_row);
  	return 0;
}
int initTerminal(){
	struct termios new_tio;
	/* get the terminal settings for stdin */
	tcgetattr(STDIN_FILENO,&old_tio);
	/* we want to keep the old setting to restore them a the end */
	new_tio=old_tio;
	/* disable canonical mode (buffered i/o) and local echo */
	new_tio.c_lflag &=(~ICANON & ~ECHO);
	/* set the new settings immediately */
	tcsetattr(STDIN_FILENO,TCSANOW,&new_tio);
	if(setvbuf(stdin,NULL,_IONBF,0)){
		return 1;
	}
	setlocale(LC_CTYPE,"UTF-8");
	return 0;
}
int printGreeting(){
	pri(1,1,L"┌");
	pri(5,1,L"└");
	pri(1,40,L"┐");
	pri(5,40,L"┘");

	for (int i=1;i<39;i++){
		pri(1,1+i,L"─");
		pri(5,1+i,L"─");

	}
	for (int i=1;i<4;i++){
		pri(1+i,1,L"│");
		pri(1+i,40,L"│");
	}
	return 0;
}
int main(){
	initTerminal();
	getWindowSize();
	printf("\e[2J\e[HHallo\n");
	printGreeting();
	wint_t w;
	while((w = getwchar()) != WEOF){
		if (w==24){
			wprintf(L"\e[2K\e[2K\r");
		}
		else {
			wprintf(L"%lc",w);
		}
	};
	/* restore the former settings */
	tcsetattr(STDIN_FILENO,TCSANOW,&old_tio);
	return 0;

}