#pragma once
#include <wchar.h>
#include<unistd.h>

struct DataStorage;
typedef struct DataStorage DataStorage;

DataStorage* create_datastorage();
int append_text(DataStorage* p_ds , const wchar_t* t);
int remove_last_char(DataStorage* p_ds);
wchar_t* get_text(DataStorage* p_ds);
void free_datastorage(DataStorage* p_ds);
size_t get_length(DataStorage* p_ds);
